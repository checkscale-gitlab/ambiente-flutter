FROM reijunior100/android-sdk

LABEL maintainer="Reinaldo Domingos  <reijunior100@@gmail.com>"
LABEL describle="Ambiente de desenvolvimento Flutter"

# Configurar variáveis de ambientes
ENV FLUTTER_URL=https://storage.googleapis.com/flutter_infra_release/releases/stable/linux/flutter_linux_2.2.3-stable.tar.xz
ENV PATH=${PATH}:/opt/flutter/bin

# Baixar e instalar dependências
RUN apt update && apt install -y git xz-utils wget unzip libgconf-2-4 gdb libstdc++6 libglu1-mesa  \
fonts-droid-fallback lib32stdc++6 python3 psmisc

# Extrair pacotes baixados
RUN curl -fsSL ${FLUTTER_URL} -o /tmp/flutter.tar.xz \
&& tar xf /tmp/flutter.tar.xz -C /opt

# Apagar instaladores e cache
RUN rm -f /tmp/flutter.tar.xz \
&& apt-get clean  \
&& apt-get autoremove -y \
&& rm -rf /var/lib/apt/lists/* \
&& rm -rf ${ANDROID_HOME}/avd/* \

# Atualizar e ativar flutter web
RUN flutter precache \
&& flutter channel master \
&& flutter upgrade \
&& flutter config --enable-web \
&& flutter config --no-analytics \

# Criar e configurar emulador android
&& flutter emulators --create --name emu${ANDROID_EMULATOR_VERSION} \
&& for f in ${ANDROID_HOME}/avd/*.avd/config.ini; do echo 'hw.keyboard=yes' >>  ${f}; done \
&& for f in ${ANDROID_HOME}/avd/*.avd/config.ini; do echo 'hw.ramSize=3072' >>  ${f}; done \
&& for f in ${ANDROID_HOME}/avd/*.avd/config.ini; do sed -i 's/hw.lcd.density=.*/hw.lcd.density=320/'  ${f}; done \
&& for f in ${ANDROID_HOME}/avd/*.avd/config.ini; do sed -i 's/hw.lcd.height=.*/hw.lcd.height=1334/'  ${f}; done \
&& for f in ${ANDROID_HOME}/avd/*.avd/config.ini; do sed -i 's/hw.lcd.width=.*/hw.lcd.width=750/'  ${f}; done \
&& echo "if [ -f /usr/src/commands_app ]; then\n . /usr/src/commands_app\nfi" >> ~/.bashrc

WORKDIR /usr/src